#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main()
 {
  int pipefd[2];
  pid_t cpid;
  int i = 0;
  char message[] = "badoutput";
  if (pipe(pipefd) == -1) {
   perror("pipe");
   exit(1);
   }

   cpid = fork();
   if (cpid == -1) {
    perror("fork");
    exit(1);
   }

   if (cpid == 0) 
   {
    close(pipefd[0]);
    strcpy(message,"magshimim");
    write(pipefd[1], message, strlen(message));
    close(pipefd[1]);
    exit(0);
    }

    else {
     close(pipefd[1]);
     while (read(pipefd[0], message, 1) > 0)
      write(STDOUT_FILENO, message, 1);
     write(STDOUT_FILENO, "\n", 1);
     close(pipefd[0]);
     wait(NULL);
     exit(0);
     }
 }

