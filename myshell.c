#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <linux/limits.h>
#include "LineParser.h"

void execute(cmdLine *command)
{
 char thisPath[PATH_MAX];
 int inOpen, outOpen, helpIn, helpOut;
 if(command->inputRedirect != NULL)
 {
  helpIn = dup(0);
  if(helpIn != -1)
  {
   close(0);
   inOpen = open(command->inputRedirect, 0);
  }
 }
 if(command->outputRedirect != NULL)
 {
  helpOut = dup(1);
  if(helpOut != -1)
  {
   close(1);
   outOpen = open(command->outputRedirect, 1);
  }
 } 
 getcwd(thisPath, PATH_MAX);
 execvp(command->arguments[0], command->arguments);
 perror("Error");
 if(helpIn != -1)
 {
  close(inOpen);
  inOpen = dup(helpIn);
  if(inOpen == 0)
  {
   close(helpIn);
  }
 }
 if(helpOut != -1)
 {
  close(outOpen);
  outOpen = dup(helpOut);
  if(outOpen == 1)
  {
   close(helpOut);
  }
 }
}

int main()
{
 int cpid, ExitF = 1, i, gage = 0;
 char path[PATH_MAX], input[2048], arr[15][2048];
 cmdLine* command;
 while(ExitF)
  {
   getcwd(path, PATH_MAX);
   printf("%s: ", path);
   fgets(input, sizeof(input), stdin);
   command = parseCmdLines(input);
   if(gage > 14)
   {
    gage = 0;
   }
   strcpy(arr[gage], input);
   gage++;
   if(input[0] == '!') strcpy(input, arr[atoi(input+1)-1]);
   command = parseCmdLines(input);
   if(!strcmp(input, "history\n"))
   {
    for(i = 0; i < 15 && strcmp(arr[i], ""); i++)
    printf("%d.%s", i+1, arr[i]);
   }
   else if(strcmp("quit", command->arguments[0]) == 0)
   {
    ExitF = 0;
   }
   else if(strcmp("cd", command->arguments[0]) == 0)
   {
    if(chdir(command->arguments[1]) < 0)
    perror("Error");
   }
   else if(strcmp("myecho", command->arguments[0]) == 0)
   {
    for(i = 1; i < command->argCount; i++)
    {
     printf("%s ",command->arguments[i]);
    }
    printf("\n");
   }
   else
   {
    cpid = fork();
    if(cpid >= 0)
    {
     if(cpid == 0)
     {
      execute(command);
      exit(0);
     }
     else
     {
      waitpid(-1, &cpid, 0);
     }
    }
    else
    {
     perror("Error");
    }
   }
  }
 freeCmdLines(command);
 return 0;
}
