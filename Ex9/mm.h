#include<unistd.h>
#include<stdio.h>
typedef struct metadata_block* p_block;

struct metadata_block
{
 size_t size;
 struct metadata_block* next;
 int free;
};

void* malloc(size_t size);
void free(void* ptr);
void* realloc(void* ptr, size_t size);
