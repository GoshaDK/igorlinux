#include<unistd.h>
#include<stdio.h>
#include "mm.h"

p_block start = NULL;

void* malloc(size_t size)
{
 void* helper;
 p_block temp = start;
 helper = (void*)sbrk(size+sizeof(p_block));
 if(!start)
 {
  temp = (p_block)helper;
  temp->free = 0;
  temp->next = NULL;
  temp->size=size;
  start = temp;
  return (temp+sizeof(p_block));
 }
 else
 {
  while(temp->next)
  {
   if((temp->free) && (temp->size == size))
   {
    temp->free = 0;
    return (temp+sizeof(p_block));
   }
   temp = temp->next;
  }
  helper = (void*)sbrk(size+sizeof(p_block));
  temp->next = (p_block)helper;
  temp = temp->next;
  temp->size = size;
  temp->free = 0;
  return(temp+sizeof(p_block));
 }
}
void free(void* ptr)
{
 p_block temp = start;
 int helper;
 while(temp->next)
 {
  if((temp+sizeof(p_block))==ptr)
  {
   temp->free = 1;
   return;
  }
  temp = temp->next;
 }
 if((temp+sizeof(p_block))==ptr)
 {
  helper = (temp->size+sizeof(p_block)) *-1;
  sbrk(helper);
  if(start == temp) start = NULL;
  return;
 }
}

void* realloc(void* ptr, size_t size)
{
 p_block helper;
 void* space;
 space = (void*)malloc(size);
 helper = ptr - sizeof(p_block);
 memcpy(space,ptr,helper->size);
 free(ptr);
 return space;
}
