#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"
#define STDOUT 1
#define STDIN 0
void fExit()
{
	syscall(SYS_exit, 0);
}
void fPrintID()
{
	syscall(SYS_write,STDOUT,"ID: ",slen("ID: "));
	syscall(SYS_write,STDOUT,__itoa(getuid()),slen(__itoa(getuid())));
	syscall(SYS_write,STDOUT,"\n",slen("\n"));
}
void fUpTime()
{
	struct sysinfo info;
	sysinfo(&info);
	syscall(SYS_write,STDOUT,"Uptime: ",slen("Uptime: "));
	syscall(SYS_write,STDOUT,__itoa(info.uptime/3600),slen(__itoa(info.uptime/3600)));
	syscall(SYS_write,STDOUT,":",slen(":"));
	syscall(SYS_write,STDOUT,__itoa(info.uptime%3600/60),slen(__itoa(info.uptime%3600/60)));
	syscall(SYS_write,STDOUT,":",slen(":"));
	syscall(SYS_write,STDOUT,__itoa(info.uptime%60),slen(__itoa(info.uptime%60)));
	syscall(SYS_write,STDOUT,"\n",slen("\n"));
	syscall(SYS_write,STDOUT,"Process count:",slen("Process count:"));
	syscall(SYS_write,STDOUT,__itoa(info.procs),slen(__itoa(info.procs)));
	syscall(SYS_write,STDOUT,"\n",slen("\n"));
}
int main()
{
	int i;
	fun_desc menu[3];
	char option[2] = "";
	menu[0].name = "1 - Exit\n";
	menu[0].fun = fExit;
	menu[1].name = "2 - Print ID\n";
	menu[1].fun = fPrintID;
	menu[2].name = "3 - UpTime\n";
	menu[2].fun = fUpTime;
	while(1)
	{
	 for(i = 0; i < 3; i++)
	 {
	  syscall(SYS_write, STDOUT, menu[i].name, slen(menu[i].name));
	 }
	 syscall(SYS_read, STDOUT, option, 2);
	 i = atoi(option);
	 switch(i)
	  {
	   case 1: menu[0].fun();break;
	   case 2: menu[1].fun();break;
	   case 3: menu[2].fun();
	  }
	}
}
