#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
void quit()
{
 char str[1024];
 printf("If You Want To Quit Type Y/y\n");
 scanf("%s",str);
 if(str[0] == 'y' || str[0] == 'Y')
 {
  exit(EXIT_SUCCESS);
 }
}

int main()
{
 int i=0;
 signal(2,quit);
 signal(3,quit);
 signal(15,quit);
 for(i=1;i<=2000000000;i++)
 {
  if(i%1000000==0) printf("\n I'm Still Alive %d",i);
 }
 exit(EXIT_SUCCESS);
 return 0;
}
