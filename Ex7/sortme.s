section .rodata
MSG:	DB	"welcome to sortMe, please sort me",10,0
S1:	DB	"%d",10,0 ; 10 = '\n' , 0 = '\0'

section .data

array	DB 5,1,7,3,4,9,12,8,10,2,6,11	; unsorted array
len	DB 12	
  
section .text
	align 16
	global main
	extern printf

main:
	push MSG	; print welcome message
	call printf
	add esp,4	; clean the stack 
	
	call printArray ;print the unsorted array
	push EBP
	mov EBP, ESP
	push EBX
	push ESI
	push EDI

	mov EAX, [len]
	mov ECX, 1
	mov ECX, EAX

	mov EAX, 1
	xor EBX, EBX
	mov ESI, [array]
	MainLoop:
		cmp EAX, ECX
		jge EndLoop
		push ECX
		mov ECX, [ESI+EAX]
		mov EBX, EAX
		dec EBX
		EnterWhile:
			cmp EBX, 0
			jl EndWhile
			cmp [ESI+EBX], ECX
			jle EndWhile
			mov EDX, [ESI+EBX]
			push EDX
			mov EDX, [ESI+EBX+1]
			pop EDX
			mov [ESI+EBX+1], EDX
			dec EBX
			jmp EnterWhile
		EndWhile:
			mov [ESI+EBX+4], ECX
			inc EAX
			pop ECX
			jmp MainLoop
	EndLoop:
	pop EDI
	pop ESI
	pop EBX
	pop EBP
	call printArray
	mov eax, 1 	;exit system call
	int 0x80

printArray:
	push ebp	;save old frame pointer
	mov ebp,esp	;create new frame on stack
	pusha		;save registers

	mov eax,0
	mov ebx,0
	mov edi,0

	mov esi,0	;array index
	mov bl,byte[len]
	add edi,ebx	; edi = array size

print_loop:
	cmp esi,edi
	je print_end
	mov al ,byte[array+esi]	;set num to print in eax
	push eax
	push S1
	call printf
	add esp,8	;clean the stack
	inc esi
	jmp  print_loop
print_end:
	popa		;restore registers
	mov esp,ebp	;clean the stack frame
	pop ebp		;return to old stack frame
	ret
