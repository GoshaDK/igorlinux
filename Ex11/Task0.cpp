#include<Windows.h>
#include<stdio.h>

void main(int argc, char** argv)
{
	DWORD n = atoi(argv[1]);
	HANDLE hndlP = OpenProcess(PROCESS_TERMINATE, TRUE, n);
	if (hndlP == NULL)
	{
		printf("There Was Error \nNumber:%u\n",GetLastError());
		system("pause");
		return;
	}
	TerminateProcess(hndlP, 0);
	CloseHandle(hndlP);
}