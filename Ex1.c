#include<signal.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main()
{
 int cpid = fork();
 if(cpid >= 0)
 {
  if(cpid == 0)
  {
   while(1){
    printf("I'm Still Alive\n");
   }
  }
  else
  {
   sleep(1);
   printf("Dispatching\n");
   kill(cpid, SIGKILL);
   printf("Dispatched\n");
  }
 }
 return 0;
}
